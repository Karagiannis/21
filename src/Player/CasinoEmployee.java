package player;

/**
 * Created by Lasse on 2015-10-27.
 */
public class CasinoEmployee extends Player{

    public CasinoEmployee(){
        this.playerType = PlayerType.casinoEmployee;
        this.playerStopsAtValue = 18;
    }
}
