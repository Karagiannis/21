package player;

import carddeck.SimplePlayCard;

import java.util.ArrayList;

/**
 * Created by Lasse on 2015-10-26.
 */
public class Player {
    public ArrayList<SimplePlayCard> hand;
    public enum PlayerStatus {lost, win, playing, waiting};
    PlayerStatus playerStatus;
    public enum PlayerType {casinoCustomer, casinoEmployee, unknown }
    public int playerStopsAtValue;
    int maxValueOfHand;
    PlayerType playerType;

     public Player() {
         this.hand = new ArrayList<SimplePlayCard>();
         this.playerType = PlayerType.unknown;
         this.maxValueOfHand = 0;
     }
     public void receivesAndPlaysCard(SimplePlayCard card) {

        this.playerStatus = PlayerStatus.playing;
        this.hand.add(card);
        if(this.hand.size() == 0){
            this.maxValueOfHand = 0;
        }else{
            int sum = 0;
            int sumWithOneAce = 0;
            int sumWithManyAce = 0;
            int numberOfAce = 0;

            for (int i = 0; i < this.hand.size(); i++ )
            {
                if (this.hand.get(i).cardValue == 1)
                    numberOfAce ++;
                if (this.hand.get(i).cardValue > 1)
                {
                    sumWithOneAce += this.hand.get(i).cardValue;
                    sumWithManyAce += this.hand.get(i).cardValue;
                }
                else if (this.hand.get(i).cardValue == 1 && numberOfAce == 1)
                {
                    sumWithOneAce += 1;
                    sumWithManyAce += 11;
                }
                else if (this.hand.get(i).cardValue == 1 && numberOfAce > 1) {
                    sumWithOneAce += 1;
                    sumWithManyAce += 1;
                }

            }//end for loop
            if (sumWithManyAce > sumWithOneAce && sumWithManyAce <= 21 ) {
                this.maxValueOfHand = sumWithManyAce;
            }
            else{
                this.maxValueOfHand = sumWithOneAce;
            }
        }
    }

    public int  valueOfHand(){
        return this.maxValueOfHand;
    }

    public void wins(){
        this.playerStatus = PlayerStatus.win;
    }

    public boolean isWinner(){
        if (this.playerStatus == PlayerStatus.win)
            return true;
        else
            return false;
    }

    public void loses(){
        this.playerStatus = PlayerStatus.lost;
    }

    public  boolean  isLooser(){
        if (this.playerStatus == PlayerStatus.lost)
            return true;
        else
            return false;
    }

    public void waits(){
        this.playerStatus = PlayerStatus.waiting;
    }

    public boolean  isWaiting(){
        if (this.playerStatus == PlayerStatus.waiting)
            return true;
        else
            return false;
    }

    public void plays(){
        this.playerStatus = PlayerStatus.playing;
    }

    public boolean  isPlaying(){
        if (this.playerStatus == PlayerStatus.playing)
            return true;
        else
            return false;
    }

    public void toDisplay(){
        for (int i = 0; i < this.hand.size(); i++){
            this.hand.get(i).toDisplay();
        }
    }

    public void resetsHisPoints(){
        this.maxValueOfHand = 0;
        int numberOfElements = this.hand.size();
       // for (Iterator i = this.hand.iterator(); i.hasNext(); )
        //    this.hand.remove(i);
        /*for (int i = 0; i < numberOfElements; i++){
            this.hand.remove(i);
        }*/
        this.hand.clear();
        this.playerStatus = PlayerStatus.waiting;
    }


}
