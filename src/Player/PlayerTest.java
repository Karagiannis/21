package player;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Lasse on 2015-10-27.
 */
public class PlayerTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testReceivesAndPlaysCard() throws Exception {

    }

    @Test
    public void testValueOfHand() throws Exception {

    }

    @Test
    public void testWins() throws Exception {

    }

    @Test
    public void testIsWinner() throws Exception {

    }

    @Test
    public void testLoses() throws Exception {

    }

    @Test
    public void testIsLooser() throws Exception {

    }

    @Test
    public void testWaits() throws Exception {

    }

    @Test
    public void testIsWaiting() throws Exception {

    }

    @Test
    public void testPlayss() throws Exception {

    }

    @Test
    public void testIsPlaying() throws Exception {

    }

    @Test
    public void testToDisplay() throws Exception {

    }

    @Test
    public void testResetsHisPoints() throws Exception {

    }
}