package player;

/**
 * Created by Lasse on 2015-10-27.
 */
public class CasinoCustomer extends Player {

    public CasinoCustomer() {
        this.playerType = PlayerType.casinoCustomer;
        this.playerStopsAtValue = 21;
    }
}
