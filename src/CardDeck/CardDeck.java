package carddeck;

public class CardDeck{

    public SimplePlayCard[] deck;
    public int arrayIndex;


    public CardDeck(){
        deck = new SimplePlayCard[52];
        arrayIndex = 51;
        String[] cardTypes;
        cardTypes =  new String [] {"spades", "hearts", "diamonds", "clubs"};


        int count = 0;
        for (int i = 0; i < cardTypes.length; i++){
            for (int j = 1; j < 14; j++){
                deck[count] = new SimplePlayCard(cardTypes[i],j);
                count++;
            }
        }
    }

    public void toDisplay(){
        for (int i = 0; i < deck.length; i++){
            deck[i].toDisplay();
        }
    }

    public void shuffle(){
        for (int i = 0; i < this.deck.length; i++){
            SimplePlayCard temp = this.deck[i];
            int randomNumber = (int)Math.floor(Math.random()*(this.deck.length -1)) +1;
            this.deck[i] =this.deck[randomNumber];
            this.deck[randomNumber]= temp;
        }
    }
    public SimplePlayCard popCard(){
        if (this.arrayIndex >= 0 && this.arrayIndex<= 51){
            return this.deck[this.arrayIndex--];
        }
        else{
            throw new Error("Cannot pop an empty deck");
        }

    }
    public void discardDeck(){
        this.shuffle();
        this.arrayIndex = 51;
    }


}
