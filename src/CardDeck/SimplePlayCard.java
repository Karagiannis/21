package carddeck;

/**
 * Created by Lasse on 2015-10-26.
 */
public class SimplePlayCard {

    public final static String spades = "SPADES";
    public final static String hearts = "HEARTS";
    public final static String diamonds = "DIAMONDS";
    public final static String clubs = "CLUBS";
    public final static String unicodeSpade= "\u9824";
    public final static String unicodeHeart =  "\u9829";
    public final static String unicodeDiamonds = "\u9830";
    public final static String unicodeClubs = "\u9827";

    public String cardType;
    public int cardValue;
    public String unicodeCardTypeSymbol;

    public SimplePlayCard(String cardType, int cardValue){


        if (cardType.charAt(0) == 's' || cardType.charAt(0) == 'S'){
            this.cardType = spades;
            this.unicodeCardTypeSymbol = unicodeSpade;
        }else if (cardType.charAt(0) == 'h' || cardType.charAt(0) == 'H'){
            this.cardType = hearts;
            this.unicodeCardTypeSymbol = unicodeHeart;
        }else if (cardType.charAt(0) == 'd' || cardType.charAt(0) == 'D'){
            this.cardType = diamonds;
            this.unicodeCardTypeSymbol = unicodeDiamonds;
        }else if (cardType.charAt(0) == 'c' || cardType.charAt(0) == 'C'){
            this.cardType = clubs;
            this.unicodeCardTypeSymbol = unicodeClubs;
        }
        else{
            throw new Error("Wrong cardtype!");
        }

        if (cardValue >0 && cardValue < 14){
            this.cardValue = cardValue;
        }else{
            throw new Error("Wrong cardvalue!");
        }

    }

   public void toDisplay(){


       if (this.cardValue < 11 && this.cardValue > 0)
            System.out.println(this.cardValue + " of  "  + this.cardType  + " " + this.unicodeCardTypeSymbol );
       else if (this.cardValue == 11)
           System.out.println( "JACK" + " of  "  + this.cardType + " " + this.unicodeCardTypeSymbol);
       else if (this.cardValue == 12)
           System.out.println( "QUEEN" + " of "  + this.cardType+ " " + this.unicodeCardTypeSymbol );
       else if (this.cardValue == 13)
           System.out.println( "KING" + " of  "  + this.cardType+ " " + this.unicodeCardTypeSymbol );
       else{
           throw new Error("Nothing to display");
       }

    }

}

