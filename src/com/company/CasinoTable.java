package com.company;

/**
 *
 * Created by Lasse on 2015-10-27.
*/

import carddeck.*;
import player.CasinoCustomer;
import player.CasinoEmployee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CasinoTable {

    private int numberOfCardsPlayedFromDeck;
    public int numberOfCustomersAtTable;
    public enum CasinoTableStates {resetAndReady,gameIsOn, stoppingGame};
    public CasinoTableStates casinoTableState;
    public CardDeck deck;
    public CasinoEmployee casinoEmployee;
    public CasinoCustomer casinoCustomer;
    public BufferedReader casinoCustomerInput;

    public CasinoTable(){

        this.numberOfCustomersAtTable = 1;
        this.casinoEmployee = new CasinoEmployee();
        this.casinoCustomer = new CasinoCustomer();
        this.deck = new CardDeck();
        this.deck.shuffle();
        this.casinoTableState = CasinoTableStates.resetAndReady;
        System.out.println("Welcome to Black Jack!\n " +
                "Press a to start a session, then c to get cards, s to stop\n" +
                "and q to guit");
        casinoCustomerInput = new BufferedReader(new InputStreamReader(System.in));

    }

    public void play() throws IOException {

        while(true){
            try {
                String customerMessage = casinoCustomerInput.readLine();
                switch(customerMessage){
                    case "q": {
                        System.out.println("quitting");
                        System.exit(0);
                        break;
                    }
                    case "a": {
                        this.gameTableResetsTheState();
                        this.gameTableSetsTheState(CasinoTableStates.gameIsOn);
                        this.casinoCustomer.plays();
                        this.casinoEmployee.waits();
                        break;
                    }
                    case "s": {
                        this.casinoCustomer.waits();
                        if (this.casinoCustomer.valueOfHand() > 0 ) {
                            System.out.println("Casino starts playing...");
                            this.casinoEmployee.plays();
                            while (this.casinoTableState != CasinoTableStates.stoppingGame) {
                                while (this.casinoCustomer.valueOfHand() > this.casinoEmployee.valueOfHand()&&
                                        this.casinoTableState != CasinoTableStates.stoppingGame ) {
                                    this.casinoEmployee.receivesAndPlaysCard(this.deck.popCard());
                                    this.gameTableEvaluatesTheGame();
                                    this.gameTableDisplaysTheGame();
                                }
                            }
                        }
                        break;
                    }
                    default:{
                        if (this.casinoTableState == CasinoTableStates.gameIsOn){
                            if (this.casinoCustomer.isPlaying()){
                                this.casinoCustomer.receivesAndPlaysCard(this.deck.popCard());
                                this.gameTableEvaluatesTheGame();
                                this.gameTableDisplaysTheGame();
                            }
                        }
                        break;
                    }
                }
            }
            catch (IOException ie){
                ie.printStackTrace();
                System.exit(1);
            }
        }


    }
    public void gameTableSetsTheState(CasinoTableStates casinoTableState){
        this.casinoTableState = casinoTableState;

    }
    public CasinoTableStates gameTableGetsTheState(){
        return this.casinoTableState;
    }

    public void gameTableResetsTheState(){
        this.casinoCustomer.resetsHisPoints();
        this.casinoEmployee.resetsHisPoints();
        this.casinoTableState = CasinoTableStates.resetAndReady;
    }



    public void gameTableDisplaysTheGame(){
        if (this.casinoCustomer.isPlaying() && this.casinoEmployee.isWaiting()){
            System.out.println("************************");
            this.casinoCustomer.toDisplay();
            System.out.println("Your points: " + this.casinoCustomer.valueOfHand() + "c, s or q?");
        }
        ///1
        else if(this.casinoCustomer.isWaiting() && this.casinoEmployee.isPlaying()){
            System.out.println("************************");
            this.casinoEmployee.toDisplay();
            System.out.println("********Casino points: " + this.casinoEmployee.valueOfHand() + "  Your points: " +
                    this.casinoCustomer.valueOfHand() + "*********");
        }
        //2
        else if(this.casinoCustomer.isLooser()&& this.casinoEmployee.valueOfHand() == 0){
            System.out.println("************************");
            this.casinoCustomer.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You lost, press a then c to continue or q to quit +++++++");
        }//3
        else if(this.casinoCustomer.isLooser()&& this.casinoTableState== CasinoTableStates.stoppingGame){
            System.out.println("************************");
            this.casinoEmployee.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                    "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You lost, press a then c to continue or q to quit +++++++");
        }//4
        else if(this.casinoCustomer.isWinner()&& this.casinoEmployee.valueOfHand()== 0){
            System.out.println("************************");
            this.casinoCustomer.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                    "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You win, press a then c to continue or q to quit +++++++");
        }
        //5
        else if(this.casinoCustomer.isWinner()&& this.casinoTableState == CasinoTableStates.stoppingGame){
            System.out.println("************************");
            this.casinoEmployee.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                    "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You win, press a then c to continue or q to quit +++++++");
        }
        //6
        else if(this.casinoEmployee.isLooser() && this.casinoTableState == CasinoTableStates.stoppingGame){
            System.out.println("************************");
            this.casinoEmployee.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                    "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You win, press a then c to continue or q to quit +++++++");
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }
        //7
        else if(this.casinoEmployee.isWinner() && this.casinoTableState == CasinoTableStates.stoppingGame){
            System.out.println("************************");
            this.casinoEmployee.toDisplay();
            System.out.println("-----Casino final points: "+ this.casinoEmployee.valueOfHand()+
                    "Your points: "+ this.casinoCustomer.valueOfHand()+"+++++++");
            System.out.println("++++++You lost, press a then c to continue or q to quit +++++++");
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }
        //8
        else{
            System.out.println("Error in display of the results of the game function");
            System.out.println("This display function has logical error. Shouldn't be able to reach this state");
            System.out.println("Hit q to quit or wait for restart of game");
            System.out.println("Restarting game...");
            this.gameTableResetsTheState();
        }
    }

    public void gameTableEvaluatesTheGame(){
        /***********   Casino Customer Game logic******************/
        if(this.casinoCustomer.isPlaying() && this.casinoCustomer.valueOfHand() > 21){
            this.casinoCustomer.loses();
            this.casinoEmployee.wins();
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//1
        else if(this.casinoCustomer.isPlaying() && this.casinoCustomer.valueOfHand() == 21){
            this.casinoCustomer.wins();
            this.casinoEmployee.loses();
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//2
        else if(this.casinoCustomer.isPlaying() && this.casinoCustomer.valueOfHand() < 21){
            this.gameTableSetsTheState(CasinoTableStates.gameIsOn);//still
            this.casinoEmployee.waits();//still
        }//3
        else if(this.casinoCustomer.isWaiting() && this.casinoEmployee.valueOfHand() < 21 &&
                this.casinoEmployee.valueOfHand() < this.casinoCustomer.valueOfHand()){
            this.casinoEmployee.plays();//still
            this.casinoCustomer.waits();//still
            this.gameTableSetsTheState(CasinoTableStates.gameIsOn);//still
        }//4
        else if(this.casinoCustomer.isWaiting() && this.casinoEmployee.valueOfHand() < 21 &&
                this.casinoEmployee.valueOfHand() >= this.casinoCustomer.valueOfHand()){
            this.casinoEmployee.wins();
            this.casinoCustomer.loses();
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//5
        else if(this.casinoCustomer.isWaiting() && this.casinoEmployee.valueOfHand() > 21){
            this.casinoEmployee.loses();
            this.casinoCustomer.wins();
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//6


        /*************CasinoEmployee Game logic***May contain code that is never reached*********/
        else if(this.casinoEmployee.isPlaying() &&
                this.casinoEmployee.valueOfHand() < casinoCustomer.valueOfHand()){
            this.casinoCustomer.waits();//Still
            this.gameTableSetsTheState(CasinoTableStates.gameIsOn);//still
        }//7
        else if(this.casinoEmployee.isPlaying() &&
                this.casinoEmployee.valueOfHand() == this.casinoCustomer.valueOfHand()){
            this.casinoEmployee.wins();
            this.casinoCustomer.loses();
            this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//8
       else if(this.casinoEmployee.valueOfHand()> this.casinoCustomer.valueOfHand() &&
                this.casinoEmployee.valueOfHand() <= 21){
                this.casinoEmployee.wins();
                this.casinoCustomer.loses();
                this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//9
        else if(this.casinoEmployee.isPlaying()&&
                this.casinoEmployee.valueOfHand()> this.casinoCustomer.valueOfHand() &&
                this.casinoEmployee.valueOfHand() > 21){
                this.casinoEmployee.loses();
                this.casinoCustomer.wins();
                this.gameTableSetsTheState(CasinoTableStates.stoppingGame);
        }//10
        else{
            System.out.println("Error in game evaluate function");
            System.out.println("this function has logical errors, shouldn't be able to reach this state");
            System.out.println("Hit q to quit or wait for restart");
            System.out.println("Restarting game...");
            this.gameTableResetsTheState();
        }
    }

}
