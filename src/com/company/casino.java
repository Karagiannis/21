package com.company;

import java.io.IOException;

public class Casino {

    public static void main(String[] args) throws IOException {

        CasinoTable greenTable = new CasinoTable();
        try {
            greenTable.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
